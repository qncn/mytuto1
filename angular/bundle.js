(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){


var DataService2 = require('./dataservice2');
var MyController2 = require('./controller2');

var app2=angular.module('myApp',[]);
//app.config value //sw version, release date, domain url 
app2.service('DataService2', DataService2);
app2.controller('MyController2', MyController2);
},{"./controller2":2,"./dataservice2":3}],2:[function(require,module,exports){
var DataService2 = require('./dataservice2');

function MyController2($scope, DataService2){
    user = DataService2.getUserById1(23);
    //DataService.getUserById2(23).then(function(data){$scope.user=data;});  ...$scope.apply
    $scope.user=user; 
}

module.exports = MyController2;
},{"./dataservice2":3}],3:[function(require,module,exports){
function DataService2($http){

    console.log(".....");
    this.getUserById1 = function(id){
        console.log("method1 called");
        user={}; //new Object();
        user.id=34;
        user.name='john';
        user.phone='123456';

    
        return user;
    };

    this.getUserById2 = function(id){
        console.log("method2 called");
        user={}; //new Object();
        user.id=34;
        user.name='john';
        user.phone='123456';

        
        //return user;
        /* 
        var returnF= function(resolve,reject){
            resolve(user);
        };
        return new Promise(returnF);
        */
        return new Promise(function(resolve,reject){
            resolve(user);
        });

    };

}



module.exports = DataService2;
/*
var dataService = require('./dataservice.js');
var dservice = new dataService();
dservice.getUserById(23).then(function(user){console.log(user);});
*/
},{}]},{},[1]);
